import { useEffect, useRef } from 'react';

const { host, protocol } = typeof window !== 'undefined' ? window?.location : {};

const onerror = (event) => {
  console.log("onerror");
  console.log(event);
};

const onclose = (event) => {
  console.log("onclose");
  console.log(event);
};

export default function useWs({ onMessages }) {
  const ws = useRef();

  const pingIntervalId = useRef(0);

  const onmessage = (event) => {
    // New incoming message from server
    console.log("onmessage")
    console.log(event)

    // Extract data from event received
    const { data } = event;
    console.log(data)

    // Handle webhook related messages (needed ???)
    if (data.substring(0, 7) === "webhook") {
      console.log("subscribed to webhook named: " + data.substring(8));
      return
    }

    // Handle error related messages
    if (data.substring(0, 5) === "error") {
      console.log(data.substring(6));
      return
    }

    // Handle pong messages
    if (data.substring(0, 4) === "pong") {
      console.log('received pong from server');
      return
    }

    // Other messages should be valid JSON
    try {
      // Make sure incoming message can be parsed as JSON
      const newMessage = JSON.parse(data);

      // Add new message to dashboard
      onMessages?.([newMessage]);
    } catch (err) {
      console.log('Incoming message cannot be parsed as JSON', err);
    }
  };

  const onopen = (event) => {
    console.log("onopen");
    console.log(event);

    // ping server
    pingIntervalId.current = setInterval(() => {
      ws.current?.send('ping');
    }, 15000);
  };

  // Handle connection / disconnection
  function connect() {
    console.log('about to connect to the WEBSOKET !!! server...');

    const token = window.localStorage.getItem('wh_token');

    // const _ws = new WebSocket(`${protocol === 'https:' ? 'wss' : 'ws'}://${host}/ws?token=${token}`)

    const _ws = new WebSocket(`${protocol === 'https:' ? 'wss' : 'ws'}://${host}/ws?token=${token}`)
 

    _ws.addEventListener('open', onopen);
    _ws.addEventListener('error', onerror);
    _ws.addEventListener('message', onmessage);
    _ws.addEventListener('close', onclose);

    return _ws;
  }

  // Clean disconnection
  function disconnect() {
    // Stop regular ping
    clearInterval(pingIntervalId);

    ws.current.removeEventListener('open', onopen);
    ws.current.removeEventListener('error', onerror);
    ws.current.removeEventListener('message', onmessage);
    ws.current.removeEventListener('close', onclose);
    ws.current.close();
  }

  useEffect(() => {
    console.log("useEffect TRY CONNECT WWEBSOKET")
    ws.current = connect();

    return () => {
      disconnect();
    };
  }, []);
}
