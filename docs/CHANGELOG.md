# 1.0.0 (2023-01-25)


### Bug Fixes

* before main ([75df5f9](https://gitlab.com/web-hook-remotejob/www/commit/75df5f9e41fc609b32c6b7a49f2309c05ae257b8))
* cd ([320d571](https://gitlab.com/web-hook-remotejob/www/commit/320d571583d3494e26d9cf8f17aa7141f171e7fd))
