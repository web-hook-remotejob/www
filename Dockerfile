FROM arm64v8/node as base
WORKDIR /app
COPY . .
ENV PORT 3000
EXPOSE 3000

FROM base as dev
ENV NODE_ENV=development
RUN npm ci
RUN mv /app/node_modules /
CMD ["npm", "run", "dev"]

FROM base as production
RUN addgroup --system --gid 10000 nodejs
RUN adduser --system --uid 10000 nextjs
ARG APP_VERSION
ENV APP_VERSION $APP_VERSION
ENV NODE_ENV=production
RUN npm ci --production
RUN npm run build
USER nextjs
CMD ["npm", "start"]