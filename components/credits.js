import Image from 'next/image'
import mypic from '../assets/civo-badge.png'

export default function Credits(props) {

  return (
    <Image
      src={mypic}
      className="credits"
      alt="Civo credits"
  />
  )
}